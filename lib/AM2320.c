#include "AM2320.h"

// test function
int AM2320_read(I2C_HandleTypeDef *hi2c, struct AM2320_DATA *data) {
	const uint8_t read_command[3] = {0x03, 0x00, 0x04};
	uint8_t rx_buff[8] = {};

	HAL_I2C_Master_Transmit(hi2c, AM2320_addr, 0, 0, 800);
	HAL_I2C_Master_Transmit(hi2c, AM2320_addr, read_command, 3, 2000);

	HAL_Delay(2);

	HAL_I2C_Master_Receive(hi2c, AM2320_addr, rx_buff, 8, 2000);

	float t = (((rx_buff[4] & 0x7F) << 8) + rx_buff[5]) / 10.0;
	data->t = ((rx_buff[4] & 0x80) >> 7) == 1 ? t * (-1) : t;
	data->h = ((rx_buff[2] << 8) + rx_buff[3]) / 10.0;

	return 0;
}
