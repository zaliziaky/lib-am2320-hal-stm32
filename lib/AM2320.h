#ifndef _AM2320_H_
#define _AM2320_H_

#include <stdint.h>
#include <stdlib.h>

#include "i2c.h"

#define AM2320_addr 0xB8

struct AM2320_DATA {
	float t;
	float h;
};


int AM2320_read(I2C_HandleTypeDef *hi2c, struct AM2320_DATA *data);

#endif
